var stage = new PIXI.Stage(0x66FF99)
  , renderer = PIXI.autoDetectRenderer(1200, 800, null, true)
  , bunnyTotal = 10
  , bunnies = []
  , maxHeight = 400
  , explosionTextures = []

var assetsToLoader = ["./SpriteSheet.json"]
  , onAssetsLoaded = function() {
      for (var i=0; i < 26; i++) {
        var texture = PIXI.Texture.fromFrame("Explosion_Sequence_A " + (i+1) + ".png");
        explosionTextures.push(texture);
      }

      creaetBunnys()
      requestAnimFrame( animate )
    }

loader = new PIXI.AssetLoader(assetsToLoader)
loader.onComplete = onAssetsLoaded
loader.load()


document.body.appendChild(renderer.view)


function createExplosion(position) {
  var explosion = new PIXI.MovieClip(explosionTextures)
  explosion.position.x = position.x - 30
  explosion.position.y = position.y - 50
  explosion.loop =false
  return explosion;
}

function creaetBunnys() {
  for (var i = 0; i < bunnyTotal; i++) {
    var texture = PIXI.Texture.fromImage("./bunny.png");
    var bunny = new PIXI.Sprite(texture);

    bunny.anchor.x = 0.5;
    bunny.anchor.y = 0.5;

    // move the sprite t the center of the screen
    bunny.position.x = 100 + i*100;
    bunny.position.y = 100;
    var gravity = Math.random()
      , factor = Math.random()

    factor = factor > 0.5 ? factor : 0.3 + factor

    bunny.data = {
      gravity: gravity > 0.5 ? gravity : 0.5
    , maxHeight: factor * maxHeight
    }

    stage.addChild(bunny);
    bunnies.push(bunny)
  }
}


function animate() {

  requestAnimFrame( animate );

  bunnies.forEach(function(bunny, index) {
    bunny.position.y += 2 * bunny.data.gravity
    if (bunny.position.y > bunny.data.maxHeight) {
      var explosion = createExplosion({
        x: bunny.position.x
      , y: bunny.position.y
      })

      stage.removeChild(bunny)
      bunnies.splice(index, 1)

      stage.addChild(explosion)
      explosion.gotoAndPlay(0)
    }
  })

  renderer.render(stage)
}
